package com.FVilbas.monthlymoneymanager.modele;

import androidx.annotation.NonNull;
import androidx.room.Entity;


//This is an associative table between Month and Operation
//It is used to manage the monthly inputs, like salary
//All the inputs associate to a month will define the Max money to use in this month

@Entity(primaryKeys = {"idMonth","idOperation"})
public class MonthInput {

    @NonNull
    private Long idMonth;

    @NonNull
    private Long idOperation;

    public Long getIdMonth() {
        return idMonth;
    }

    public Long getIdOperation() {
        return idOperation;
    }

    public void setIdMonth(@NonNull Long idMonth) {
        this.idMonth = idMonth;
    }

    public void setIdOperation(@NonNull Long idInput) {
        this.idOperation = idInput;
    }
}
