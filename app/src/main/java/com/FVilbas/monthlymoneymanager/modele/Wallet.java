package com.FVilbas.monthlymoneymanager.modele;

import androidx.annotation.NonNull;
import androidx.room.*;

import java.util.List;

@Entity(tableName = "wallet",
        indices = {@Index("idWallet"), @Index("value_max")})
public class Wallet {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "idWallet")
    private int idWallet;

    @ColumnInfo(name = "name")
    @NonNull
    private String name;

    @ColumnInfo(name = "value_max")
    private double valueMax;


    public Wallet(@NonNull String name, double valueMax) {
        this.name = name;
        this.valueMax = valueMax;
    }

    public int getIdWallet() {
        return idWallet;
    }

    public void setIdWallet(int id) {
        this.idWallet = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public double getValueMax() {
        return valueMax;
    }

    public void setValueMax(double valueMax) {
        this.valueMax = valueMax;
    }



}
