package com.FVilbas.monthlymoneymanager.modele;

import androidx.annotation.NonNull;
import androidx.room.Entity;

//Associative table between Month and wallet

@Entity(primaryKeys = {"idWallet","idMonth"})
public class MonthWallManyToMany {

    @NonNull
    private Long idWallet;

    @NonNull
    private Long idMonth;

    public Long getIdWallet() {
        return idWallet;
    }

    public void setIdWallet(Long idWall) {
        this.idWallet = idWall;
    }

    public Long getIdMonth() {
        return idMonth;
    }

    public void setIdMonth(Long idMonth) {
        this.idMonth = idMonth;
    }
}
