package com.FVilbas.monthlymoneymanager.modele;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "operation",
        indices = {@Index("idOperation"), @Index("name"), @Index("every_month")})
public class Operation {

    @PrimaryKey(autoGenerate = true)
    private Long idOperation;

    public void setIdOperation(Long id) {
        this.idOperation = id;
    }

    public Operation(String name, double value, boolean everymonth, String detail) {
        this.name = name;
        this.value = value;
        this.everymonth = everymonth;
        this.detail = detail;
    }

    private String name;

    private double value;

    private String detail;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @ColumnInfo(name = "every_month")
    private boolean everymonth;

    public boolean isEverymonth() {
        return everymonth;
    }

    public void setEverymonth(boolean everymonth) {
        this.everymonth = everymonth;
    }

    public Long getIdOperation() {
        return idOperation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
