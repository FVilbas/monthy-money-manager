package com.FVilbas.monthlymoneymanager.modele;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;

//associative table between month and operation

@Entity(primaryKeys = {"idWallet","idOperation"})
public class OpWallManyToMany {

    @NonNull
    private Long idWallet;

    @NonNull
    public Long getIdWallet() {
        return idWallet;
    }

    public void setIdWallet(@NonNull Long idWallet) {
        this.idWallet = idWallet;
    }

    @NonNull
    public Long getIdOperation() {
        return idOperation;
    }

    public void setIdOperation(@NonNull Long idOperation) {
        this.idOperation = idOperation;
    }

    @NonNull
    private Long idOperation;
}
