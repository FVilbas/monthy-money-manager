package com.FVilbas.monthlymoneymanager.modele;

import androidx.annotation.NonNull;
import androidx.room.*;

import java.util.List;


@Entity(tableName = "month", indices ={ @Index(value = {"the_month","year"}, unique = true),
        @Index("idMonth"), @Index("the_month"), @Index("year")})
public class Month {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "idMonth")
    private Long idMonth;

    @ColumnInfo(name = "year")
    private int year;

    @ColumnInfo(name = "the_month")
    private int month;

    public Month(int year, int month) {
        this.year = year;
        this.month = month;
    }

    public Long getIdMonth() {
        return idMonth;
    }

    public void setIdMonth(Long id) {
        this.idMonth = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
}
