package com.FVilbas.monthlymoneymanager.database;

import android.content.Context;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.FVilbas.monthlymoneymanager.dao.MonthDAO;
import com.FVilbas.monthlymoneymanager.dao.OperationDAO;
import com.FVilbas.monthlymoneymanager.dao.WalletDAO;
import com.FVilbas.monthlymoneymanager.modele.*;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Month.class, Operation.class,Wallet.class,MonthWallManyToMany.class,OpWallManyToMany.class,MonthInput.class},
        version = 1)
public abstract class MMMDatabase extends RoomDatabase {


    private static final String DB_NAME = "MMMDatabase.db";
    private static final int NUMBER_OF_THREADS = 4;
    public static Executor databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);;
    private static volatile MMMDatabase instance;

    public static synchronized MMMDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static MMMDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                MMMDatabase.class,
                DB_NAME).addCallback(roomCallback)
                .build();
    }

    private final static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new DefaultDB(instance).execute();
        }
    };

    private static class DefaultDB extends AsyncTask<Void,Void,Void> {
        private OperationDAO operationDAO;

        private DefaultDB(MMMDatabase db){
            operationDAO = db.getOperationDAO();

        }
        @Override
        protected Void doInBackground(Void... voids) {
            Operation defaultSalary = new Operation("salary", 1000, true, "input") {
            };
            operationDAO.add(defaultSalary);
            return null;
        }
    }




    public abstract OperationDAO getOperationDAO();
    public abstract MonthDAO getMonthDAO();
    public abstract WalletDAO getWalletDAO();



}
