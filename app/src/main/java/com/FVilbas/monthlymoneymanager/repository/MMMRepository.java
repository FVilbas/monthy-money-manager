package com.FVilbas.monthlymoneymanager.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.FVilbas.monthlymoneymanager.dao.MonthDAO;
import com.FVilbas.monthlymoneymanager.dao.OperationDAO;
import com.FVilbas.monthlymoneymanager.dao.WalletDAO;
import com.FVilbas.monthlymoneymanager.database.MMMDatabase;
import com.FVilbas.monthlymoneymanager.modele.Month;
import com.FVilbas.monthlymoneymanager.modele.Operation;

import java.util.Date;

public class MMMRepository {

    private WalletDAO walletDAO;
    private OperationDAO operationDAO;
    private MonthDAO monthDAO;

    MMMDatabase db;

    MMMRepository(Application application) {
       db = MMMDatabase.getInstance(application);

        monthDAO = db.getMonthDAO();
        walletDAO = db.getWalletDAO();
        operationDAO = db.getOperationDAO();


    }

    public LiveData<Month> getMonthListInput(Long idMonth){
        return null;
    }

    public LiveData<Month> getMonthNow(){
        Date now = new Date();
        int year = now.getYear();
        int month = now.getMonth();

        return monthDAO.findMonthByYearAndMonth(year, month);
    }

    void insertOperation(final Operation operation) {
        MMMDatabase.databaseWriteExecutor.execute(() -> {
            operationDAO.add(operation);
        });
    }
}
