package com.FVilbas.monthlymoneymanager.dao;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import com.FVilbas.monthlymoneymanager.modele.Operation;

import java.util.List;

@Dao
public interface OperationDAO {

    //Update Operation in db
    @Update
    void update(Operation operation);

    //Delete an operation in db
    @Delete
    void delete(Operation operation);

    //Insert operation in db
    @Insert
    void add(Operation operation);

    //Find an opeartion from id
    @Query("select * from operation where idOperation = :id")
    Operation find(Long id);

    //Find all operation for a month
    //Only direct operation, not in wallet
    //Used for month inputs (salary etc...)
    @Query("select o.* from operation o inner join monthinput mi on mi.idOperation = o.idOperation" +
            " inner join month m on m.idMonth = mi.idMonth where m.idMonth = :id and o.detail like 'input'")
    LiveData<List<Operation>> findInputbyMonthId(Long id);

    //Find all operations in a wallet
    @Query("select o.* from operation o inner join opwallmanytomany wm on wm.idOperation = o.idOperation" +
            " inner join wallet w on w.idWallet = wm.idWallet where w.idWallet = :id")
    LiveData<List<Operation>> findOperationbyWalletId(Long id);

    //Link a month and an operation
    //Uses associative table MonthInput
    @Transaction
    @Query("insert into MonthInput (idOperation, idMonth) values (:idOperation, :idMonth) ")
    void addOperationToMonth(Long idOperation, Long idMonth);

    //Add an operation to a wallet
    //Uses OpWallManyToMany associative table
    @Transaction
    @Query("insert into OpWallManyToMany (idWallet, idOperation) values (:idWallet, :idOperation) ")
    void addOperationToWallet(Long idWallet, Long idOperation);

}
