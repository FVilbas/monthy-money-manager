package com.FVilbas.monthlymoneymanager.dao;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import com.FVilbas.monthlymoneymanager.modele.Month;
import com.FVilbas.monthlymoneymanager.modele.Operation;
import com.FVilbas.monthlymoneymanager.modele.Wallet;

import java.util.List;

@Dao
public interface WalletDAO {


    @Query("select * from wallet where idWallet = (:id)")
    LiveData<Wallet> findWallet(Long id);

    @Update
    void update (Wallet wallet);

    @Delete
    void delete(Wallet wallet);

    @Insert
    void insert(Wallet wallet);

    @Query("select w.* from wallet w inner join monthwallmanytomany mw on mw.idWallet = w.idWallet" +
            " inner join month m on m.idMonth = mw.idMonth where m.idMonth = :id")
    LiveData<List<Wallet>> findWalletByMonthId(Long id);


    @Query("insert into MonthWallManyToMany (idWallet, idMonth) values (:idWallet, :idMonth) ")
    void addWalletToMonth(Long idWallet, Long idMonth);
}
