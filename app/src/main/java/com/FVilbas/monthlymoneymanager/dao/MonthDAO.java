package com.FVilbas.monthlymoneymanager.dao;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import com.FVilbas.monthlymoneymanager.modele.Month;

@Dao
public interface MonthDAO {

    //find a month from the id
    @Query("select * from month where idMonth = (:id)")
    LiveData<Month> findMonth(Long id);

    //add a new month
    @Insert
    void add(Month month);

    //update a month
    @Update
    void update(Month month);

    //Find month from the couple year/month
    @Query("select * from month where year = :year and the_month = :month")
    LiveData<Month> findMonthByYearAndMonth(int year, int month);





}
